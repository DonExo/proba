FROM python:3.9-slim-buster

WORKDIR /project

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . .


# CMD ["flask", "run", "--host", "0.0.0.0", "--port", "5000"]
# CMD ["python", "app.py"]